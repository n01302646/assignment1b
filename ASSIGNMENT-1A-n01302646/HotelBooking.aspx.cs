﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASSIGNMENT_1A_n01302646
{
    public partial class HotelBooking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CIdatePicker.Visible = false;
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            CIdatePicker.Visible = true;
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            CIdate.Text = CIdatePicker.SelectedDate.ToShortDateString();
            CIdatePicker.Visible = false;
        }

        protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date.CompareTo(DateTime.Today) < 0)
            {
                e.Day.IsSelectable = false;
            }
        }

        protected void Booking(object sender, EventArgs e)
        {
            info.InnerHtml = "Thank you for the booking";

            string roomtype = TypeOfRoom.SelectedItem.Value.ToString();
            int numberOfGuests = int.Parse(NoOfGuests.Text);
            double stayDays = double.Parse(DaysToStay.Text);
            DateTime CheckInDate = DateTime.Parse(CIdate.Text);
            List<String> foodAvailed = new List<String> {"Breakfast","Snacks" };

            Room newroom = new Room(CheckInDate, foodAvailed, roomtype, numberOfGuests, stayDays);

            string CustName = Fname.Text.ToString();
            string CustEmail = Email.Text.ToString();
            double CustPhone = double.Parse(PhoneNo.Text);
            Customer newcustomer = new Customer();
            newcustomer.CName = CustName;
            newcustomer.CPhoneNo = CustPhone;
            newcustomer.CEmail = CustEmail;
            
            //By Using Logic built by Christine
            List<string> foodAvailedList = new List<string>();

            foreach (Control control in Food.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox foods = (CheckBox)control;
                    if (foods.Checked)
                    {
                        foodAvailedList.Add(foods.Text);
                    }

                }
            }
            newroom.foodAvailed = newroom.foodAvailed.Concat(foodAvailedList).ToList();
            Booking newbooking = new Booking(newroom, newcustomer);

            DivBookingConf.InnerHtml = newbooking.Confirmation();
        }
        }          
}