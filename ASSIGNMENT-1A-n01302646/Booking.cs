﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASSIGNMENT_1A_n01302646
{
    public class Booking
    {
        public Room room;
        public Customer customer;

        public Booking(Room r,Customer c)
        {
            room = r;
            customer = c;
        }

        public string Confirmation()
        {
            string yourBooking = "Booking Confirmation:<br>";
            yourBooking += "THANNKS FOR THE BOOKING<br/><br/>";
            yourBooking += "Total Booking Amount:" + CalculateBookingTotal().ToString() + "<br/>";
            yourBooking += "Name: " + customer.CName + "<br/>";
            yourBooking += "Email: " + customer.CEmail + "<br/>";
            yourBooking += "Phone Number: " + customer.CPhoneNo.ToString() + "<br/>";

            yourBooking += "Check-In Date: " + room.CheckInDate + "<br/>";
            yourBooking += "Stay days: " + room.stayDays.ToString() + "<br/>";
            yourBooking += "Room Type: " + room.typeOfRoom + "<br/>";
            yourBooking += "No Of Guests" + room.noOfGuests.ToString() + "<br/>";     
            yourBooking += "Food Availed: " + String.Join(" ", room.foodAvailed.ToArray()) + "<br/>";
            yourBooking += "AND YOUR BOOKING TOTAL IS :" + CalculateBookingTotal().ToString() + "<br/>";
            return yourBooking;
        }

        public double CalculateBookingTotal()
        {

            double roomTotal = 0;
            double foodTotal = 0;
            if (room.typeOfRoom == "SingleBed")
            {
                roomTotal = 80*room.stayDays;
            }
            else
            {
                roomTotal = 100 * room.stayDays;
            }

            foodTotal = room.foodAvailed.Count() * 12 * room.stayDays;
            return (roomTotal+foodTotal);
        }

    }
}