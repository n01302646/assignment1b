﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASSIGNMENT_1A_n01302646
{
    public class Customer
    {
            private string C_Name;
            private double C_PhoneNo;
            private string C_Email;
            public Customer()
            {

            }
            public string CName
            {
                get { return C_Name; }
                set { C_Name = value; }
            }
            public double CPhoneNo
            {
                get { return C_PhoneNo; }
                set { C_PhoneNo = value; }
            }
            public string CEmail
            {
                get { return C_Email; }
                set { C_Email = value; }
            }

    }
}
