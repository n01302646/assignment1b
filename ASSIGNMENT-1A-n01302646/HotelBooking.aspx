﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="ASSIGNMENT_1A_n01302646.HotelBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>   
    
    <form id="form1" runat="server">
        <h2>Room Booking</h2>
       <asp:ValidationSummary  ID="validationSumary" runat="server"/>
    <div>
        <asp:Label runat="server" Text="Label">WELCOME TO JASON'S HOTEL <br/><br/> </asp:Label>
       <br/><asp:Label runat="server" Text="Label">First, Last Name:</asp:Label>
        <asp:TextBox ID="Fname" runat="server"></asp:TextBox>
           <asp:RequiredFieldValidator runat="server" id="reqName" controltovalidate="Fname" errormessage="Please enter your name!" />

         <asp:Label runat="server" Text="Label"><br/>Check IN Date:</asp:Label>
        <asp:TextBox ID="CIdate" runat="server"></asp:TextBox>
        <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" ImageUrl="~/Images/img1.png" Width="21px" />
        
        <asp:Calendar ID="CIdatePicker" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" OnDayRender="Calendar1_DayRender" OnSelectionChanged="Calendar1_SelectionChanged" Width="200px">
            <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
            <NextPrevStyle VerticalAlign="Bottom" />
            <OtherMonthDayStyle ForeColor="#808080" />
            <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
            <SelectorStyle BackColor="#CCCCCC" />
            <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
            <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
            <WeekendDayStyle BackColor="#FFFFCC" />
        </asp:Calendar>
        <br />
        <asp:Label  runat="server" Text="Label"><br/>No of days you want to stay:  </asp:Label> 
         <asp:TextBox ID="DaysToStay" runat="server"></asp:TextBox>  

         <br /><br /> <asp:RadioButtonList runat="server" ID="TypeOfRoom">
                <asp:ListItem Text="SingleBed" >Single Bed</asp:ListItem>
                <asp:ListItem Text="DoubleBed" >Double Bed</asp:ListItem>
            </asp:RadioButtonList>

         <br /><br/><asp:Label  runat="server" Text="Label">Number of guests</asp:Label>
        <asp:TextBox ID="NoOfGuests" runat="server"></asp:TextBox>
          <asp:RangeValidator ID="RangeValidator1" runat="server"  ControlToValidate="NoOfGuests" ErrorMessage=" "
            MaximumValue="20" ></asp:RangeValidator> 
        
        <br /><br /><asp:Label runat="server" Text="Label">E-Mail:</asp:Label>
        <asp:TextBox ID="Email" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ErrorMessage="Enter a valid E mail"></asp:RegularExpressionValidator>
         
        <br /><br /><asp:Label  runat="server" Text="Label">Phone No</asp:Label>
        <asp:TextBox ID="PhoneNo" runat="server"></asp:TextBox>

       <br /><br /> <asp:Label runat="server" Text="Label">FOOD</asp:Label>
         <asp:CheckBoxList ID="Food" runat="server">
        <asp:ListItem Value="Breakfast"> Breakfast </asp:ListItem>
        <asp:ListItem Value="Lunch"> Lunch </asp:ListItem>
        <asp:ListItem Value="Dinner"> Dinner </asp:ListItem>
        <asp:ListItem Value="Snacks"> Snacks </asp:ListItem>
         </asp:CheckBoxList>
       

       <br /><br /> 
      
        <br/><asp:Button ID="Button1" runat="server" OnClick="Booking" Text="SUBMIT"/>
        
    </div>
    </form>
    <div id="info" runat="server">

    </div>
    <div id="DivBookingConf" runat="server">

    </div>
    <div id="Div2" runat="server">

    </div>
</body>
</html>
