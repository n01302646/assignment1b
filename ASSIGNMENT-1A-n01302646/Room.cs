﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASSIGNMENT_1A_n01302646
{
    public class Room
    {
        public List<string> foodAvailed;
        public string typeOfRoom;
        public int noOfGuests;
        public DateTime CheckInDate;
        public double stayDays;

        public Room(DateTime cid, List<string> f, string tor, int nog, double sd)
        {
            foodAvailed = f;
            typeOfRoom = tor;
            noOfGuests = nog;
            CheckInDate = cid;
            stayDays = sd;
        }
    }
}