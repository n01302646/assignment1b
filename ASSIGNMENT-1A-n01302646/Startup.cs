﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASSIGNMENT_1A_n01302646.Startup))]
namespace ASSIGNMENT_1A_n01302646
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
